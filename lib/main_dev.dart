import 'package:flutter/material.dart';
import './resources/app_config.dart';
import 'main.dart';

void main() {
  const configuredApp = AppConfig(
    child: MyApp(),
    appTitle: "Flutter Flavor Dev",
    buildFlavor: "Development",
  );
  return runApp(configuredApp);
}
