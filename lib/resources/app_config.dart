import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

class AppConfig extends InheritedWidget {
  final Widget child;
  final String appTitle;
  final String buildFlavor;

  const AppConfig(
      {Key? key,
      required this.child,
      required this.appTitle,
      required this.buildFlavor})
      : super(key: key, child: child);

  static AppConfig? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppConfig>();
  }

  @override
  bool updateShouldNotify(AppConfig oldWidget) {
    return false;
  }
}
